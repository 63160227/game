class Weapon {
  String? _name = "";
  double? _atk = 0.0;
  String? _type = "";
  double? _cost = 0.0;
  int? _durability = 0;
  int _used_durability = 0;
  int? _amount = 0;

  Weapon({
    String? name,
    double? atk,
    String? type,
    double? cost,
    int? amount,
    int? durability,
  }) {
    this._name = name;
    this._atk = atk;
    this._type = type;
    this._cost = cost;
    this._amount = amount;
    this._durability = durability;
    _used_durability = durability!;
  }

  String? getName() {
    return this._name;
  }

  String? getType() {
    return this._type;
  }

  double? getAtk() {
    return this._atk;
  }

  double? getCost() {
    return this._cost;
  }

  int? getAmount() {
    return this._amount;
  }

  int? getDurability() {
    return this._durability;
  }

  int? getUsedDurability() {
    return this._used_durability;
  }

  void decreaseDurability(int amount) {
    _used_durability -= amount;
  }

  void decreaseAmount(int amount) {
    _amount = (_amount! - amount);
  }

  void setName(String? name) {
    this._name = name;
  }

  bool isOwner(String? owner) {
    return true;
  }

  bool isOutOfStock() {
    if (_amount == 0) return true;
    return false;
  }

  bool isCostEnough(double? cost) {
    if (_cost! <= cost!) return true;
    return false;
  }

  bool existWeapon(String? name) {
    if (name == _name) return true;
    return false;
  }

  String toString() {
    return '($_name) ATK : $_atk DURABILITY : $_used_durability / $_durability | TYPE : $_type | COST : $_cost\$ | AMOUNT : $_amount';
  }

  void setFullDuration() {
    _used_durability = _durability!;
  }
}
