import 'Inventory.dart';
import 'Weapon.dart';

class Player {
  String? _name;
  double? _atk;
  double? _def;
  double? _hp;
  double? _money;
  Inventory? _inventory;
  int zombieKill = 0;

  Player(String? name, double? atk, double? def, double? hp, double? money,
      Inventory? inventory) {
    _name = name;
    _atk = atk;
    _def = def;
    _hp = hp;
    _money = money;
    _inventory = inventory;
  }

  String? getName() {
    return _name;
  }

  void setName(String name) {
    _name = name;
  }

  void increaseKills(int n) {
    zombieKill += n;
  }

  String? getPlayerStatus() {
    return '\tName: $_name\n O\tHP: $_hp\n/|\\\tATK: $_atk\n | \tZombie Kills: $zombieKill\n/ \\\tMoney: $_money\$';
  }

  Inventory? getInventory() {
    return _inventory;
  }

  bool addWeaponToInventory(Weapon weapon) {
    if (weapon.isOutOfStock()) {
      return false;
    }
    if (!_inventory!.existWeapon(weapon.getName()!)) {
      if (_money! >= weapon.getCost()!) {
        _money = _money! - weapon.getCost()!;
        _inventory!.insertWeapon(weapon);
        return true;
      }
    }
    return false;
  }

  double? getMoney() {
    return _money;
  }

  void decreaseMoney(double money) {
    _money = (_money! - money);
  }

  double? getHp() {
    return _hp;
  }

  void increaseHP(double hp) {
    _hp = (_hp! + hp);
  }

  void decreaseHP(double hp) {
    _hp = (_hp! - hp);
  }

  void increaseATK(double atk) {
    _atk = (_atk! + atk);
  }

  void decreaseATK(double atk) {
    _atk = (_atk! - atk);
  }

  void increaseDEF(double def) {
    _def = (_def! + def);
  }

  void decreaseDEF(double def) {
    _def = (_def! - def);
  }

  void increaseMoney(double money) {
    _money = (_money! + money);
  }

  bool isDead() {
    if (_hp! <= 0) {
      return true;
    }
    return false;
  }
}
