import 'dart:io';
import 'Game.dart';

void main() {
  Game game = Game();
  String? input;

  game.welcomeGame();
  print("_______________________________________________\n");
  game.ruleWelcomeGame();
  print("_______________________________________________\n");

  stdout.write("Enter player name : ");
  game.setPlayerFirst(stdin.readLineSync()!);

  print("\nChoose the section that the player wants ");
  while (true) {
    if (game.playerIsDead()) {
      print('''=============== Game Over ==================
              You Are Dead.
=============================================''');
      // print('\tYou are dead.');
      break;
    }
    game.sectionMenu();
    stdout.write("\nEnter menu: ");
    input = stdin.readLineSync()!;
    if (input.isNotEmpty) {
      if (int.parse(input) > game.sectionSize()) {
        print(
            "\nSorry! This item does not exist in the system. Please select again.\n");
      } else {
        game.processInput(int.parse(input));
      }
    }
  }
}
