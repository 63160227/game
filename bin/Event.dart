import 'dart:math';
import 'dart:io';

class Event {
  int? _no;
  List<String> _dialog = [];
  String? _prize;
  String? _money_prize;
  String? _weapon_prize;
  int _bad_luck = 0;

  Event(int? no, List<String> dialog, String? prize, String? money_prize,
      String? weapon_prize, int bad_luck) {
    _no = no;
    _dialog = dialog;
    _prize = prize;
    _money_prize = money_prize;
    _weapon_prize = weapon_prize;
    _bad_luck = bad_luck;
  }

  int? getNumberEvent() {
    return _no;
  }

  String? getPrize() {
    return _prize;
  }

  int getLuck() {
    Random random = Random();
    return random.nextInt(100);
  }

  bool isBadLuck() {
    int luck = getLuck();
    if (_bad_luck >= luck) {
      return true;
    }
    return false;
  }

  int? getBadLuck() {
    return _bad_luck;
  }

  bool foundZombies() {
    Random random = Random();
    int chance = random.nextInt(2);
    if (chance != 1) {
      return true;
    }
    return false;
  }

  String convertLuckToDialogString() {
    return 'Dialog';
  }

  List<String> getDialog() {
    return _dialog;
  }

  int? getRandomZombies(int turn) {
    Random random = Random();
    int chance =
        ((random.nextInt(2) + 1) * (turn / (random.nextInt(4) + 2)).round());
    if (chance == 0) chance = 1;
    return chance;
  }

  bool canRunAwayFromZombies() {
    int rate = 20;
    Random random = Random();
    int chance = random.nextInt(100) + 1;
    if (chance <= rate) {
      return true;
    }
    return false;
  }

  bool isFoundSomething() {
    if (_prize! == 'None') {
      return false;
    }
    return true;
  }

  List<int> process() {
    List<int> list = [0, 0];
    Random random = Random();
    String select = '';
    if (_prize == 'Event') {
      while (true) {
        stdout.write('YES (Y)/ No(N) : ');
        select = stdin.readLineSync()!;
        if (select.isNotEmpty) {
          if (select == 'Y') {
            return [
              int.parse(_weapon_prize!.split(',')[random.nextInt(1)]),
              int.parse(_money_prize!)
            ];
          } else if (select == 'N') {
            return [-1, 0];
          } else {
            print('NO ARGUEMENT, Please try again');
          }
        }
      }
    } else if (_prize == 'Money') {
      return [0, int.parse(_money_prize!)];
    } else if (_prize == 'Bad') {
      return [1, int.parse(_money_prize!)];
    } else if (_prize == 'HPUP') {
      return [2, int.parse(_money_prize!)];
    }
    return [0, 0];
  }
}
