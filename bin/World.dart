import 'Event.dart';
import 'dart:math';

class World {
  int? _turn = 0;
  List<Event> _events = [];

  World() {
    _turn = 1;
    _initialEvent();
  }

  void nextTurn() {
    _turn = _turn! + 1;
  }

  int? getTurn() {
    return _turn;
  }

  Event randomEvent() {
    int length = _events.length;
    Random random = Random();
    int index_random = random.nextInt(length);
    return _events.elementAt(index_random);
  }

  void _initialEvent() {
    Random random = Random();
    _events.addAll([
      Event(1, ['Dialog Test', 'All right'], 'Money', '1', '', 100),
      Event(2, ['You found money on the ground 2\$'], 'Money', '2', '', 60),
      Event(
          3, ['You got a animal trap. You will lost 2HP'], 'Bad', '2', '', 60),
      Event(4, ['You found a weird water. Do yo want to drink it ?'], 'Event',
          random.nextInt(3).toString(), '1,2', 60),
      Event(5, ['You need to go home now.'], 'None', '0', '', 60),
      Event(6, ['You found a enery drink. Your HP have increase 5 HP'], 'HPUP',
          '5', '', 60),
      Event(7, ['You found a strange box. Will you open it or ignore it?'],
          'Event', random.nextInt(3).toString(), '0,1', 60),
      Event(
          8,
          [
            'You come across a mysterious tunnel. to walk in or go the other way'
          ],
          'Event',
          random.nextInt(3).toString(),
          '2,-1',
          60)
    ]);
  }

  // 0 ได้เงิน
  // 1 ลดเลือด
  // 2 เพิ่มเลือด
  // -1 ไม่มีอะไรเกิดขึ้น
}
