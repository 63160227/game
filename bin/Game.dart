import 'Event.dart';
import 'Player.dart';
import 'World.dart';
import 'Weapon.dart';
import 'Inventory.dart';
import "dart:io";
import "dart:math";

class Game {
  Player? _player;
  World? _world;
  List<Weapon>? _weapon_shop;
  List<num> potion = [5, 6.5];
  List<String> _section = [];

  Game() {
    Inventory inventory = Inventory();
    _player = Player('Name', 1.0, 1.0, 10.0, 10, inventory);
    _world = World();
    _section = [
      "(1) Go adventure.",
      "(2) Upgrade skills or buy weapons",
      "(3) View player status",
      "(4) Buy HP Potion ${potion[0]} HP <= ${potion[1]}\$"
    ];
    inventory.insertWeapon(Weapon(
        name: 'Knife',
        atk: 1.0,
        type: 'Weapon',
        cost: 0.0,
        amount: 1,
        durability: 5));
    _weapon_shop = _initialWeaponShop();
  }

  String introDialog() {
    return 'Hello';
  }

  void welcomeGame() {
    List<String> str = [
      'Welcome to zombieland !!',
      'Congratulations, you are a survivor of the collapse that turned humans into zombies.'
    ];
    _runningDialog(str);
    // _runningDialog(str, speedText: 0, waitText: 0);
  }

  void ruleWelcomeGame() {
    List<String> choices = [
      "1 => Players must survive as long as possible.",
      "2 => Players can buy weapons and upgrade their skills.",
      "3 => On the daily commute Players will face different situations. Some days may be lucky and some days may encounter bad luck",
      "4 => Players can view their own stats and weapons."
    ];
    print("steps to survival\n");
    _runningDialog(choices, speedText: 15, waitText: 1000);
    // _runningDialog(choices, speedText: 0, waitText: 0);
  }

  bool playerIsDead() {
    return _player!.isDead();
  }

  void sectionMenu() {
    _runningDialog(_section, speedText: 1, waitText: 200);
    // _runningDialog(_section, speedText: 0, waitText: 0);
  }

  int sectionSize() {
    return _section.length;
  }

  void setPlayerFirst(String name) {
    _player?.setName(name);
  }

  void lookupStatus() {
    print(_player?.getPlayerStatus());
  }

  void adventure() {
    List<String> dialog = ['Turn ${_world!.getTurn()}'];
    print('\n\n\n\n\n\n\n\n\n\n\n\n');
    _runningDialog(dialog, waitText: 1000, speedText: 100);

    Event event = _world!.randomEvent();

    if (event.foundZombies()) {
      int? zombiesN = event.getRandomZombies(_world!.getTurn()!);
      _runningDialog(
          ['You found the $zombiesN zombies, now what next do you do ?'],
          waitText: 200, speedText: 20);
      while (true) {
        if (playerIsDead()) {
          break;
        }
        print('COMMAND: Fight (1) | Run (2)');
        stdout.write('SELECT: ');
        String select = stdin.readLineSync()!;

        if (select == '1') {
          bool isKill = true;
          while (true) {
            print('================= Inventory =================');
            for (Weapon wp in _player!.getInventory()!.getWeapons()) {
              print(wp.toString());
            }
            stdout
                .write('Type Weapon name (SELECT FIRST WEAPON IF NO VALUE): ');
            String weapon_name = stdin.readLineSync()!;
            if (weapon_name.isEmpty) {
              if (_player!.getInventory()!.getWeapons().isEmpty) {
                print("\nSorry, You don't have anything to fight with zombies");
                _player!.decreaseHP(zombiesN!.toDouble());
                _runningDialog([
                  'You has attack by zombie $zombiesN times. Now you have ${_player!.getHp()} HP'
                ], speedText: 20, waitText: 1000);
                isKill = false;
                break;
              }
              Weapon a = _player!.getInventory()!.getFirstWeapon();
              if (a.getUsedDurability()! - zombiesN! < 0) {
                print('\nYour weapon has broken');
                _player!.getInventory()!.removeWeapon(a);
                double yHP = (zombiesN - a.getUsedDurability()!).toDouble();
                _player!.decreaseHP(yHP);
                print(
                    'You has attack by zombie $yHP times. Now you have ${_player!.getHp()} HP');
                zombiesN = a.getUsedDurability()!;
              } else {
                _player!
                    .getInventory()!
                    .getFirstWeapon()
                    .decreaseDurability(zombiesN);
              }

              break;
            } else if (_player!.getInventory()!.existWeapon(weapon_name)) {
              Weapon wp = _player!
                  .getInventory()!
                  .getWeapons()
                  .singleWhere((element) => element.getName() == weapon_name);
              if (wp.getUsedDurability()! - zombiesN! < 0) {
                print('\nYour weapon has broken');
                _player!.getInventory()!.removeWeapon(wp);
                double yHP = (zombiesN - wp.getUsedDurability()!).toDouble();
                _player!.decreaseHP(yHP);
                print(
                    'You has attack by zombie $yHP times. Now you have ${_player!.getHp()} HP');
                zombiesN = wp.getUsedDurability()!;
              } else {
                _player!
                    .getInventory()!
                    .getWeapons()
                    .singleWhere((element) => element.getName() == weapon_name)
                    .decreaseDurability(zombiesN);
                if (_player!
                        .getInventory()!
                        .getWeapons()
                        .singleWhere(
                            (element) => element.getName() == weapon_name)
                        .getUsedDurability() ==
                    0) {
                  print('\nYour weapon has broken');
                  _player!.getInventory()!.removeWeapon(wp);
                }
              }
              break;
            }
          }
          print('');
          if (isKill) {
            _player!.increaseKills(zombiesN);
            _player!.increaseMoney(zombiesN.toDouble());
            _runningDialog([
              'Congrat!!, You has killed $zombiesN zombies, and money earn $zombiesN\$'
            ], waitText: 2000, speedText: 20);
          }
          break;
        } else if (select == '2') {
          if (event.canRunAwayFromZombies()) {
            _player!.decreaseHP(1.00);
            _runningDialog([
              "You can't run from zombies. You has attack by zombie 1 times. Now you have ${_player!.getHp()} HP"
            ], waitText: 1000, speedText: 20);
            print('\n\n\n');
          } else {
            _runningDialog(["\nWhewww, You can run away from zombie"],
                waitText: 1000, speedText: 20);
            break;
          }
        } else {
          print('NO ARGUEMENT, Please try again.\n');
        }
      }
    } else {
      _runningDialog(["Whewww, Today you didn't see zombies"],
          speedText: 20, waitText: 1000);
    }
    if (event.isFoundSomething()) {
      _runningDialog(['But !!'], waitText: 1000, speedText: 20);
      _runningDialog(event.getDialog(), waitText: 200, speedText: 20);
      List<int> newEvent = event.process();
      if (newEvent[0] == 0) {
        // Increase Prize
        _player!.increaseMoney(newEvent[1].toDouble());
        _runningDialog([
          'Congrat!!, You got ${newEvent[1].toDouble()}\$',
          'Now you have ${_player!.getMoney()}\$'
        ], waitText: 1000, speedText: 20);
      } else if (newEvent[0] == 1) {
        // Decrease HP
        _player!.decreaseHP(newEvent[1].toDouble());
        _runningDialog([
          'Oh no, Your HP has decrease ${newEvent[1].toDouble()}',
          'Now you have ${_player!.getHp()} HP'
        ], speedText: 20, waitText: 1000);
      } else if (newEvent[0] == 2) {
        // Increase HP
        _player!.increaseHP(newEvent[1].toDouble());
        _runningDialog([
          'Nice, Your HP has increase ${newEvent[1].toDouble()}',
          'Now you have ${_player!.getHp()} HP'
        ], speedText: 20, waitText: 1000);
      } else if (newEvent[0] == -1) {
        // Nothing
        _runningDialog(['Okay, Go home now.'], speedText: 20, waitText: 1000);
      }
    } else {
      _runningDialog(['Today you not found anything.'],
          waitText: 1000, speedText: 20);
    }

    print('\n\n\n\n\n\n\n\n\n\n');
    _world!.nextTurn();
  }

  void weaponShopEntry() {
    String select = '';
    int total = _weapon_shop!.length;
    int page = 0;
    int limit = 5;
    int total_page = (total / limit).ceil();

    while (true) {
      print('================ Welcome to Shop ================');
      int offset = limit * (page);
      List<Weapon?> wp_page = _weapon_shop!.sublist(
          offset,
          (offset + limit) > _weapon_shop!.length
              ? _weapon_shop!.length
              : (offset + limit));
      for (Weapon? wp in wp_page) {
        print(wp.toString());
      }
      print(
          '\nPage ${page + 1} / $total_page (Type "Weapon Name" to buy) (MONEY: ${_player!.getMoney()})');
      stdout.write('Next Page(n/N) / Previous Page(b/B) / Exit(q/Q) : ');
      select = stdin.readLineSync()!;
      if (['n', 'N'].contains(select)) {
        if (page + 1 < total_page) {
          page++;
        }
      } else if (['b', 'B'].contains(select)) {
        if (page > 0) {
          page--;
        }
      } else if (['q', 'Q'].contains(select)) {
        break;
      } else {
        int id =
            _weapon_shop!.indexWhere((element) => element.existWeapon(select));
        if (id >= 0) {
          Weapon select_weapon = _weapon_shop!.elementAt(id);
          if (_player!.getMoney()! < select_weapon.getCost()!) {
            _runningDialog(['Failed, Your money not enough to buy the weapon'],
                speedText: 20, waitText: 1000);
          } else if (_player!
              .getInventory()!
              .existWeapon(select_weapon.getName()!)) {
            // Shopping
            Weapon shop = _weapon_shop!.singleWhere(
                (element) => select_weapon.getName() == element.getName());
            // Inventory
            Weapon a = _player!.getInventory()!.getWeapons().singleWhere(
                (element) => select_weapon.getName() == element.getName());

            _runningDialog(['Success, Your weapon has full durability'],
                speedText: 20, waitText: 1000);
            int diff = a.getDurability()! - a.getUsedDurability()!;
            double per = (shop.getDurability()! / shop.getCost()!);
            a.setFullDuration();
            double sum = diff.toDouble() * per;
            _player!.decreaseMoney(sum.ceilToDouble());
          } else if (_player!.addWeaponToInventory(select_weapon)) {
            print('${select_weapon.getCost()}, ${_player!.getMoney()}');
            _weapon_shop!.elementAt(id).decreaseAmount(1);
            _runningDialog(['Success, weapon is put in your inventory now.'],
                speedText: 20, waitText: 1000);
          } else {
            print('Failed, Please try again.');
          }
        } else {
          print('Sorry, Not Found Weapon');
        }
      }
    }
  }

  bool _validInput({String? input, List<dynamic>? list}) {
    bool valid = false;
    for (var data in list!) {
      if (input!.toLowerCase() == data.toString().toLowerCase()) {
        valid = true;
      }
    }
    return valid;
  }

  void exitGame() {
    print('Bye Bye');
  }

  void processInput(int? n) {
    switch (n) {
      case 1:
        adventure();
        break;
      case 2:
        weaponShopEntry();
        break;
      case 3:
        print('================ Player Status ================');
        lookupStatus();
        for (Weapon wp in _player!.getInventory()!.getWeapons()) {
          print(wp.toString());
        }
        print("_______________________________________________\n");
        break;
      case 4:
        if (_player!.getMoney()! < potion[1]) {
          _runningDialog(['Failed, Your money not enough to buy the potion.'],
              speedText: 20, waitText: 1000);
        } else {
          _player!.decreaseMoney(potion[1].toDouble());
          _player!.increaseHP(potion[0].toDouble());
          _runningDialog(['Success, Now you have ${_player!.getHp()} HP.'],
              speedText: 20, waitText: 1000);
        }
        break;
      default:
    }
  }

  void printInventory() {
    List<Weapon>? wp = _player?.getInventory()?.getWeapons();
    for (Weapon w in wp!) {
      print(w.toString());
    }
  }

  void _runningDialog(List<String> dialog, {speedText = 40, waitText = 1000}) {
    for (String element in dialog) {
      for (int i = 0; i < element.length; i++) {
        stdout.write(element[i]);
        sleep(Duration(milliseconds: speedText));
      }
      sleep(Duration(milliseconds: waitText));
      print("");
    }
  }

  List<Weapon>? _initialWeaponShop() {
    return [
      Weapon(
          name: 'Knife',
          atk: 1.0,
          type: 'Melee',
          cost: 7.0,
          amount: 1,
          durability: 10),
      Weapon(
          name: 'Glock16',
          atk: 2.0,
          type: 'Pistol',
          cost: 15.0,
          amount: 5,
          durability: 20),
      Weapon(
          name: 'M4',
          atk: 1.0,
          type: 'Assalt Rifle',
          cost: 30.0,
          amount: 5,
          durability: 45),
      Weapon(
          name: 'MP5',
          atk: 1.0,
          type: 'SMG',
          cost: 25.0,
          amount: 5,
          durability: 35),
      Weapon(
          name: 'AK47',
          atk: 1.0,
          type: 'Assalt Rifle',
          cost: 40.0,
          amount: 5,
          durability: 55),
      Weapon(
          name: 'Bison',
          atk: 1.0,
          type: 'SMG',
          cost: 28.0,
          amount: 5,
          durability: 38),
      Weapon(
          name: 'Bat',
          atk: 1.0,
          type: 'Melee',
          cost: 10.0,
          amount: 5,
          durability: 15)
    ];
  }
}
