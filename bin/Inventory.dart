import 'Weapon.dart';

class Inventory {
  String _name = "";
  int _amount = 0;
  List<Weapon> _weapons = [];

  String getName() {
    return _name;
  }

  int getAmount() {
    return _amount;
  }

  List<Weapon> getWeapons() {
    return _weapons;
  }

  void insertWeapon(Weapon wp) {
    _weapons.add(wp);
  }

  void removeWeapon(Weapon wp) {
    _weapons.remove(wp);
  }

  Weapon getFirstWeapon() {
    return _weapons.first;
  }

  bool existWeapon(String name) {
    if (_weapons.any((element) => element.getName() == name)) {
      return true;
    }
    return false;
  }
}
